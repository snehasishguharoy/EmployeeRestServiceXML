/**
 * 
 */
package com.delta.cru.rest.poc.dao;

import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mybatis.spring.SqlSessionTemplate;

import com.delta.cru.rest.poc.mapper.EmployeeMapper;
import com.delta.cru.rest.poc.vo.EmpVo;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
// @ContextConfiguration(locations = { "classpath*:applicationContext.xml" })
public class EmpDaoImplTest {
	public static BasicDataSource dataSource;
	@InjectMocks
	private EmpDaoImpl daoImpTest;
	@Mock
	private EmployeeMapper employeeMapper;

	// @Before
	// public void setUp() throws Exception {
	// create();
	// daoImpTest = new EmpDaoImpl();
	//
	// }
	//
	// public static void create() throws Exception {
	// try {
	// final SimpleNamingContextBuilder builder = new
	// SimpleNamingContextBuilder();
	// dataSource = new BasicDataSource();
	// dataSource.setUsername("SYSTEM");
	// dataSource.setPassword("ADMIN");
	// dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	// dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:xe");
	// builder.bind("jdbc/oracle", dataSource);
	// builder.activate();
	// } catch (NamingException ex) {
	// }
	// }

	@Test
	public void fEmpByIdTest() {

		List<EmpVo> empVos = daoImpTest.fEmpById();
		Assert.assertNotNull(empVos);

	}

	@Test
	public void findEmpByIdTest() {
		// PowerMockito.mockStatic(EmpDaoImpl.class);
		// Mockito.when(MyBatisSqlSessionFactory.getSqlSessionFactory().openSession()).thenReturn(session);

		EmpVo empVo = daoImpTest.findEmpById(101);
		Assert.assertNull(empVo);

	}

}
