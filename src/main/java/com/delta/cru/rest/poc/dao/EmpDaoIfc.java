/**
 * 
 */
package com.delta.cru.rest.poc.dao;

import java.util.List;

import com.delta.cru.rest.poc.vo.EmpVo;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
public interface EmpDaoIfc {
	
	/**
	 * F emp by id.
	 *
	 * @return the list
	 */
	List<EmpVo> fEmpById();
	
	/**
	 * Find emp by id.
	 *
	 * @param empId the emp id
	 * @return the emp vo
	 */
	EmpVo findEmpById(int empId);


}
