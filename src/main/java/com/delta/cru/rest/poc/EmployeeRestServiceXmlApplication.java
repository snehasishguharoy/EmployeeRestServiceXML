package com.delta.cru.rest.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * The Class EmployeeRestServiceXmlApplication.
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
public class EmployeeRestServiceXmlApplication extends SpringBootServletInitializer {
	private static final Class<EmployeeRestServiceXmlApplication> applicationClass = EmployeeRestServiceXmlApplication.class;

	public static void main(String[] args) {
		SpringApplication.run(EmployeeRestServiceXmlApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

}
