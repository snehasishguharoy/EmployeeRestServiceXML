package com.delta.cru.rest.poc.mapper;

import java.util.List;

import com.delta.cru.rest.poc.vo.EmpVo;

public interface EmployeeMapper {

	/**
	 * Find all emps.
	 *
	 * @return the list
	 */

	List<EmpVo> findAllEmps();

	/**
	 * Find emp by id.
	 *
	 * @param emplId
	 *            the empl id
	 * @return the emp vo
	 */
	EmpVo findEmpById(int emplId);

}
