/**
 * 
 */
package com.delta.cru.rest.poc.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.delta.cru.rest.poc.dao.EmpDaoIfc;
import com.delta.cru.rest.poc.exp.EmployeeNotFoundException;
import com.delta.cru.rest.poc.vo.EmpVo;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
@Component
public class EmpDtlsImpl implements EmpDtlsIfc {
	
	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(EmpDtlsImpl.class);

	/** The emp dao ifc. */
	@Autowired
	private EmpDaoIfc employeeDaoImpl;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.delta.cru.rest.poc.bo.EmpDtlsIfc#findEmployeeById()
	 */
	@Override
	public List<EmpVo> findEmployeeById() throws EmployeeNotFoundException {
		log.info("#### findAllEmployees");
//		if (null != empDaoIfc.fEmpById()) {
//			throw new EmployeeNotFoundException("Employee Details not found");
//		}
		return employeeDaoImpl.fEmpById();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.delta.cru.rest.poc.bo.EmpDtlsIfc#findEmpById(int)
	 */
	@Override
	public EmpVo findEmpById(int empId) throws EmployeeNotFoundException {
//		if (null != empDaoIfc.findEmpById(empId)) {
//			throw new EmployeeNotFoundException("Employee Details not found for the empId:" + empId);
//		}
		return employeeDaoImpl.findEmpById(empId);
	}


}
