//package com.delta.cru.rest.poc.dao.utils;
//
//import javax.naming.NamingException;
//
//import org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS;
//import org.apache.commons.dbcp.datasources.SharedPoolDataSource;
//import org.springframework.mock.jndi.SimpleNamingContextBuilder;
//
//public class JndiBean {
//	public JndiBean() {
//		try {
//			
//			DriverAdapterCPDS cpds = new DriverAdapterCPDS();
//			cpds.setDriver("oracle.jdbc.driver.OracleDriver");
//			cpds.setUrl("jdbc:oracle:thin:@localhost:1521:xe");
//			cpds.setUser("SYSTEM");
//			cpds.setPassword("ADMIN");
//
//			SharedPoolDataSource dataSource = new SharedPoolDataSource();
//			dataSource.setConnectionPoolDataSource(cpds);
//			dataSource.setMaxActive(10);
//			dataSource.setMaxWait(50);
//
//			SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
//			builder.bind("java:comp/env/jdbc/oracle", dataSource);
//			builder.activate();
//		} catch (NamingException | ClassNotFoundException ex) {
//			ex.printStackTrace();
//		}
//	}
//}
